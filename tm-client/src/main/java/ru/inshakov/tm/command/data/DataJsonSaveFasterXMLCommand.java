package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

public class DataJsonSaveFasterXMLCommand extends AbstractCommand {

    @Nullable

    public String name() {
        return "data-save-json-f";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save data to JSON by FasterXML.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataJson(getSession());
    }

}