package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.ProjectAbstractCommand;
import ru.inshakov.tm.endpoint.Project;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "projectGraph-show-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show projectGraph by index.";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectByIndex(getSession(), index);
        if (project == null) throw new ProjectNotFoundException();
        show(project);
    }
}
