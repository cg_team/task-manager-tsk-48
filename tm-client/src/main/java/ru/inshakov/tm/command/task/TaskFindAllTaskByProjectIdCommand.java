package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllTaskByProjectIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-find-by-projectGraph-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all task in projectGraph by projectGraph id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = serviceLocator.getTaskEndpoint().findTaskByProjectId(getSession(), id);
        System.out.println("Task list for projectGraph");
        for (@NotNull Task task : tasks) {
            System.out.println(task.toString());
        }
    }
}
