package java.ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.inshakov.tm.dto.Project;
import ru.inshakov.tm.dto.User;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.service.ConnectionService;
import ru.inshakov.tm.service.PropertyService;
import ru.inshakov.tm.service.dto.ProjectService;
import ru.inshakov.tm.service.dto.UserService;

import java.ru.inshakov.tm.marker.DBCategory;
import java.util.List;

public class ProjectServiceTest {

    @Nullable
    private static ProjectService projectService;

    @Nullable
    private static User user;

    @Nullable
    private Project project;


    @BeforeClass
    public static void beforeClass() {
        projectService = new ProjectService(new ConnectionService(new PropertyService()));
        @NotNull final UserService userService =
                new UserService(new ConnectionService(new PropertyService()), new PropertyService());
        userService.add("test", "test");
        user = userService.findByLogin("test");
    }

    @Before
    public void before() {
        project = projectService.add(user.getId(), new Project("test_project"));
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("test_project", project.getName());

        @NotNull final Project projectById = projectService.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectService.findAll(user.getId());
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectService.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final Project project = projectService.findById(user.getId(), this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Project project = projectService.findById(user.getId(), "1234");
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final Project project = projectService.findById(user.getId(), null);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final Project project = projectService.findById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        projectService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @NotNull final Project project = projectService.findByName(user.getId(), "test_project");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Project project = projectService.findByName(user.getId(), "1234");
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @NotNull final Project project = projectService.findByName(user.getId(), null);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectService.findByName("test", this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        @NotNull final Project project = projectService.findByIndex(user.getId(), 1);
        Assert.assertNotNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrect() {
        @NotNull final Project project = projectService.findByIndex(user.getId(), 34);
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void findByIndexNull() {
        @NotNull final Project project = projectService.findByIndex(user.getId(), null);
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrectUser() {
        @NotNull final Project project = projectService.findByIndex("test", 0);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        projectService.removeById(user.getId(), project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        projectService.removeById(user.getId(), null);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrect() {
        projectService.removeByIndex(user.getId(), 34);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void removeByIndexNull() {
        projectService.removeByIndex(user.getId(), null);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrectUser() {
        projectService.removeByIndex("test", 0);
    }


    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        projectService.removeByName(user.getId(), null);
    }


}
