package java.ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.system.AccessDeniedException;
import ru.inshakov.tm.service.ConnectionService;
import ru.inshakov.tm.service.PropertyService;
import ru.inshakov.tm.service.dto.SessionService;
import ru.inshakov.tm.service.dto.UserService;

import java.ru.inshakov.tm.marker.DBCategory;
import java.util.List;

public class SessionServiceTest {

    @Nullable
    private static SessionService sessionService;

    @Nullable
    private static Session session;

    @BeforeClass
    public static void beforeClass() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final UserService userService = new UserService(connectionService, new PropertyService());
        userService.add("test", "test");
        sessionService = new SessionService(
                connectionService,
                userService,
                new PropertyService());
    }

    @Before
    public void before() {
        session = sessionService.open("test", "test");
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());

        @NotNull final Session sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session.getId(), sessionById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final Session session = sessionService.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Session session = sessionService.findById("1234");
        Assert.assertNull(session);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        sessionService.findById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionService.findAllByUserId(this.session.getUserId());
        Assert.assertNotNull(session);
        Assert.assertTrue(session.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Session> session = sessionService.findAllByUserId("1234");
        Assert.assertNotNull(session);
        Assert.assertNotEquals(1, session.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdNull() {
        @NotNull final List<Session> session = sessionService.findAllByUserId(null);
        Assert.assertNull(session);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        sessionService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        sessionService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void close() {
        sessionService.close(session);
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void closeAllByUserId() {
        sessionService.closeAllByUserId(session.getUserId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = AccessDeniedException.class)
    public void validateIncorrect() {
        sessionService.validate(new Session());
    }

    @Test
    @Category(DBCategory.class)
    public void open() {
        @NotNull final Session session = sessionService.open("test", "test");
        Assert.assertNotNull(session);
    }

    @Category(DBCategory.class)
    @Test(expected = AccessDeniedException.class)
    public void openIncorrect() {
        sessionService.open("test", "tes");
    }

    @Test
    @Category(DBCategory.class)
    public void validate() {
        @NotNull final Session session = sessionService.open("test", "test");
        sessionService.validate(session);
    }

    @Category(DBCategory.class)
    @Test(expected = AccessDeniedException.class)
    public void validateChanged() {
        @NotNull final Session session = sessionService.open("test", "test");
        session.setSignature("7");
        sessionService.validate(session);
    }

}