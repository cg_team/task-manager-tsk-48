package java.ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.inshakov.tm.dto.User;
import ru.inshakov.tm.exception.empty.EmptyEmailException;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyLoginException;
import ru.inshakov.tm.exception.empty.EmptyPasswordException;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.service.ConnectionService;
import ru.inshakov.tm.service.PropertyService;
import ru.inshakov.tm.service.dto.UserService;

import java.ru.inshakov.tm.marker.DBCategory;
import java.util.List;

public class UserServiceTest {

    @Nullable
    private UserService userService;

    @Nullable
    private User user;

    @Before
    public void before() {
        userService = new UserService(new ConnectionService(new PropertyService()), new PropertyService());
        @NotNull final User user = new User();
        user.setLogin("test");
        user.setEmail("test_email");
        user.setPasswordHash("123456");
        this.user = userService.add(user);
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("test_email", user.getEmail());

        @NotNull final User userById = userService.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user.getId(), userById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<User> users = userService.findAll();
        Assert.assertTrue(users.size() > 1);
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final User user = userService.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final User user = userService.findById("1234");
        Assert.assertNull(user);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final User user = userService.findById(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() {
        @NotNull final User user = userService.findByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByLoginIncorrect() {
        @NotNull final User user = userService.findByLogin("1234");
        Assert.assertNull(user);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyLoginException.class)
    public void findByLoginNull() {
        @NotNull final User user = userService.findByLogin(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByEmail() {
        @NotNull final User user = userService.findByEmail(this.user.getEmail());
        Assert.assertNotNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByEmailIncorrect() {
        @NotNull final User user = userService.findByEmail("1234");
        Assert.assertNull(user);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyEmailException.class)
    public void findByEmailNull() {
        @NotNull final User user = userService.findByEmail(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        userService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        userService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void setPassword() {
        @NotNull final User user = userService.setPassword(this.user.getId(), "password");
        Assert.assertNotNull(user);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void setPasswordUserIdNull() {
        @NotNull final User user = userService.setPassword(null, "password");
        Assert.assertNotNull(user);
    }

    @Category(DBCategory.class)
    @Test(expected = UserNotFoundException.class)
    public void setPasswordUserIdIncorrect() {
        @NotNull final User user = userService.setPassword("null", "password");
        Assert.assertNotNull(user);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyPasswordException.class)
    public void setPasswordNull() {
        @NotNull final User user = userService.setPassword("null", null);
        Assert.assertNotNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void isLoginExist() {
        Assert.assertTrue(userService.isLoginExist(this.user.getLogin()));
    }

    @Test
    @Category(DBCategory.class)
    public void isLoginExistFalse() {
        Assert.assertFalse(userService.isLoginExist("test"));
    }

    @Test
    @Category(DBCategory.class)
    public void isEmailExist() {
        Assert.assertTrue(userService.isEmailExist(user.getEmail()));
    }

    @Test
    @Category(DBCategory.class)
    public void isEmailExistFalse() {
        Assert.assertFalse(userService.isEmailExist("emai"));
    }

}