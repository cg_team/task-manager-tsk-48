package ru.inshakov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@MappedSuperclass
public abstract class AbstractBusinessGraphEntity extends AbstractGraphEntity {

    @Nullable
    @ManyToOne
    protected UserGraph userGraph;

}