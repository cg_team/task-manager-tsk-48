package ru.inshakov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.dto.ISessionRepository;
import ru.inshakov.tm.dto.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractDtoRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Session> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM SessionDto e WHERE e.userId = :userId", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDto e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDto e", Session.class).getResultList();
    }

    public Session findById(@Nullable final String id) {
        return entityManager.find(Session.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDto e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        Session reference = entityManager.getReference(Session.class, id);
        entityManager.remove(reference);
    }
}