package ru.inshakov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    @NotNull
    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
