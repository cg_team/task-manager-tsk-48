package ru.inshakov.tm.constant;

public interface FieldConst {

    String ID = "id";

    String USER_ID = "user_id";

    String NAME = "name";

    String DESCRIPTION = "description";

    String STATUS = "status";

    String START_DATE = "start_date";

    String FINISH_DATE = "finish_date";

    String CREATED = "created";

    String PROJECT_ID = "project_id";

    String TIMESTAMP = "timestamp";

    String SIGNATURE = "signature";

    String LOGIN = "login";

    String PASSWORD_HASH = "password_hash";

    String EMAIL = "email";

    String FIRST_NAME = "first_name";

    String LAST_NAME = "last_name";

    String MIDDLE_NAME = "middle_name";

    String ROLE = "role";

    String LOCKED = "locked";
}
