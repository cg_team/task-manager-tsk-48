package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.ServiceLocator;
import ru.inshakov.tm.api.service.dto.IProjectService;
import ru.inshakov.tm.api.service.dto.IProjectTaskService;
import ru.inshakov.tm.api.service.model.IProjectGraphGraphService;
import ru.inshakov.tm.dto.Project;
import ru.inshakov.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint {

    private IProjectService projectDtoService;

    private IProjectTaskService ProjectTaskService;

    private IProjectGraphGraphService projectService;

    public ProjectEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IProjectService projectDtoService,
            @NotNull final IProjectTaskService ProjectTaskService,
            @NotNull final IProjectGraphGraphService projectService
    ) {
        super(serviceLocator);
        this.projectDtoService = projectDtoService;
        this.ProjectTaskService = ProjectTaskService;
        this.projectService = projectService;
    }

    @WebMethod
    public List<Project> findProjectAll(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionDtoService().validate(session);
        List<Project> list = projectDtoService.findAll(session.getUserId());
        return list;
    }

    @WebMethod
    public void addProjectAll(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "collection") final Collection<Project> collection
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        projectDtoService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public Project addProject(
            @WebParam(name = "session") final Session session, @WebParam(name = "entity") final Project entity
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.add(session.getUserId(), entity);
    }

    @WebMethod
    public Project addProjectWithName(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.add(session.getUserId(), name, description);
    }

    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearProject(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionDtoService().validate(session);
        projectService.clear(session.getUserId());
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session") final Session session, @WebParam(name = "entity") final Project entity
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        projectDtoService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public Project startProjectById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.startById(session.getUserId(), id);
    }

    @WebMethod
    public Project startProjectByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project startProjectByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public Project finishProjectById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public Project finishProjectByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project finishProjectByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return projectDtoService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session") final Session session, @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        projectService.removeById(session.getUserId(), projectId);
    }

    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        projectDtoService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        projectDtoService.removeByName(session.getUserId(), name);
    }
}
