package ru.inshakov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.ServiceLocator;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Nullable
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
