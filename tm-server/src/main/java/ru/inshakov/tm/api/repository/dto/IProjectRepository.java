package ru.inshakov.tm.api.repository.dto;

import ru.inshakov.tm.dto.Project;

import java.util.List;

public interface IProjectRepository {

    void add(final Project project);

    void update(final Project project);

    Project findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<Project> findAllByUserId(final String userId);

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    List<Project> findAll();

    Project findById(final String id);

    void clear();

    void removeById(final String id);

}
