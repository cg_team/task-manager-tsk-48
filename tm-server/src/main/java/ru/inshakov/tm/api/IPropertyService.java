package ru.inshakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.other.ISaltSetting;
import ru.inshakov.tm.api.other.ISignatureSetting;

public interface IPropertyService extends ISaltSetting, ISignatureSetting {

    String getApplicationVersion();

    @NotNull String getServerHost();

    @NotNull String getServerPort();

    @Nullable String getJdbcUser();

    @Nullable String getJdbcPassword();

    @Nullable String getJdbcUrl();

    @Nullable String getJdbcDriver();

    @Nullable String getHibernateDialect();

    @Nullable String getHibernateBM2DDLAuto();

    @Nullable String getHibernateShowSql();

    @Nullable String getSecondLevelCash();

    @Nullable String getQueryCache();

    @Nullable String getMinimalPuts();

    @Nullable String getLiteMember();

    @Nullable String getRegionPrefix();

    @Nullable String getCacheProvider();

    @Nullable String getFactoryClass();
}
