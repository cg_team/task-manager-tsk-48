package ru.inshakov.tm.api.service.model;

import ru.inshakov.tm.api.IGraphService;
import ru.inshakov.tm.model.UserGraph;

public interface IUserGraphGraphService extends IGraphService<UserGraph> {

    UserGraph findByLogin(final String login);

    UserGraph findByEmail(final String email);

    void removeByLogin(final String login);

    UserGraph add(final String login, final String password);

    UserGraph add(final String login, final String password, final String email);

    UserGraph setPassword(final String id, final String password);

    boolean isLoginExist(final String login);

    boolean isEmailExist(final String email);

    UserGraph updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    );

    UserGraph lockByLogin(final String login);

    UserGraph unlockByLogin(final String login);

}
