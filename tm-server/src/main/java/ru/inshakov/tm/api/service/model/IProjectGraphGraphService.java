package ru.inshakov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IGraphService;
import ru.inshakov.tm.model.ProjectGraph;
import ru.inshakov.tm.model.UserGraph;

import java.util.Collection;
import java.util.List;

public interface IProjectGraphGraphService extends IGraphService<ProjectGraph> {

    ProjectGraph findByName(final String userId, final String name);

    ProjectGraph findByIndex(final String userId, final Integer index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final Integer index);

    ProjectGraph updateById(final String userId, final String id, final String name, final String description);

    ProjectGraph updateByIndex(final String userId, final Integer index, final String name, final String description);

    ProjectGraph startById(final String userId, final String id);

    ProjectGraph startByIndex(final String userId, final Integer index);

    ProjectGraph startByName(final String userId, final String name);

    ProjectGraph finishById(final String userId, final String id);

    ProjectGraph finishByIndex(final String userId, final Integer index);

    ProjectGraph finishByName(final String userId, final String name);

    ProjectGraph add(UserGraph userGraph, String name, String description);

    List<ProjectGraph> findAll(@NotNull String userId);

    void addAll(UserGraph userGraph, @Nullable Collection<ProjectGraph> collection);

    ProjectGraph add(UserGraph userGraph, @Nullable ProjectGraph entity);

    ProjectGraph findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable ProjectGraph entity);
}
