package ru.inshakov.tm.api.repository.dto;

import ru.inshakov.tm.dto.User;

import java.util.List;

public interface IUserRepository {

    User findByLogin(final String login);

    User findByEmail(final String email);

    void removeUserByLogin(final String login);

    void add(final User user);

    void update(final User user);

    List<User> findAll();

    User findById(final String id);

    void clear();

    void removeById(final String id);

}
