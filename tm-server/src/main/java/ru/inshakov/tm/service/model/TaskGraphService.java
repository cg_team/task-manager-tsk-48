package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.model.ITaskGraphGraphRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.model.ITaskGraphGraphService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.TaskGraph;
import ru.inshakov.tm.model.UserGraph;
import ru.inshakov.tm.repository.model.TaskGraphGraphRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class TaskGraphService extends AbstractGraphService<TaskGraph> implements ITaskGraphGraphService {

    @NotNull
    public TaskGraphService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public ITaskGraphGraphRepository getRepository() {
        return new TaskGraphGraphRepository(connectionService.getEntityManager());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskGraph> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<TaskGraph> collection) {
        if (collection == null) return;
        for (TaskGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph add(@Nullable final TaskGraph entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    private void update(@NotNull final TaskGraph entity, @NotNull EntityManager entityManager) {
        @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
        repository.update(entity);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            @NotNull final List<TaskGraph> taskGraphs = repository.findAll();
            for (TaskGraph t :
                    taskGraphs) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            @Nullable final TaskGraph taskGraph = repository.getReference(optionalId.orElseThrow(EmptyIdException::new));
            if (taskGraph == null) return;
            repository.remove(taskGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final TaskGraph entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            @Nullable final TaskGraph taskGraph = repository.getReference(entity.getId());
            if (taskGraph == null) return;
            repository.remove(taskGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            return taskRepository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @Nullable final TaskGraph taskGraph = taskRepository.findByIndex(userId, index);
            if (taskGraph == null) return;
            taskRepository.remove(taskGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @Nullable final TaskGraph taskGraph = taskRepository.findByName(userId, name);
            if (taskGraph == null) return;
            taskRepository.remove(taskGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @NotNull final TaskGraph taskGraph = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            taskGraph.setName(name);
            taskGraph.setDescription(description);
            update(taskGraph, entityManager);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @NotNull final TaskGraph taskGraph = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            taskGraph.setName(name);
            taskGraph.setDescription(description);
            update(taskGraph, entityManager);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @NotNull final TaskGraph taskGraph = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            taskGraph.setStatus(Status.IN_PROGRESS);
            taskGraph.setStartDate(new Date());
            update(taskGraph, entityManager);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @NotNull final TaskGraph taskGraph = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            taskGraph.setStatus(Status.IN_PROGRESS);
            taskGraph.setStartDate(new Date());
            update(taskGraph, entityManager);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @NotNull final TaskGraph taskGraph = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            taskGraph.setStatus(Status.IN_PROGRESS);
            taskGraph.setStartDate(new Date());
            update(taskGraph, entityManager);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @NotNull final TaskGraph taskGraph = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            taskGraph.setStatus(Status.COMPLETED);
            taskGraph.setFinishDate(new Date());
            update(taskGraph, entityManager);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @NotNull final TaskGraph taskGraph = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            taskGraph.setStatus(Status.COMPLETED);
            taskGraph.setFinishDate(new Date());
            update(taskGraph, entityManager);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository taskRepository = new TaskGraphGraphRepository(entityManager);
            @NotNull final TaskGraph taskGraph = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            taskGraph.setStatus(Status.COMPLETED);
            taskGraph.setFinishDate(new Date());
            update(taskGraph, entityManager);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Nullable
    public TaskGraph add(@NotNull UserGraph userGraph, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskGraph taskGraph = new TaskGraph(name, description);
        add(userGraph, taskGraph);
        return (taskGraph);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskGraph> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final UserGraph userGraph, @Nullable final Collection<TaskGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (TaskGraph item : collection) {
            item.setUserGraph(userGraph);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph add(@NotNull final UserGraph userGraph, @Nullable final TaskGraph entity) {
        if (entity == null) return null;
        entity.setUserGraph(userGraph);
        @Nullable final TaskGraph entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            repository.clearByUserId(userId);
            @NotNull final List<TaskGraph> taskGraphs = repository.findAllByUserId(userId);
            for (TaskGraph t :
                    taskGraphs) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            @Nullable final TaskGraph taskGraph = repository.findByIdUserId(
                    userId,
                    optionalId.orElseThrow(EmptyIdException::new)
            );
            if (taskGraph == null) return;
            repository.remove(taskGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final TaskGraph entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskGraphGraphRepository repository = new TaskGraphGraphRepository(entityManager);
            @Nullable final TaskGraph taskGraph = repository.findByIdUserId(userId, entity.getId());
            if (taskGraph == null) return;
            repository.remove(taskGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
