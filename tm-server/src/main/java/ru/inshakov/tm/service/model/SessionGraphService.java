package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.repository.model.ISessionGraphGraphRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.model.ISessionGraphGraphService;
import ru.inshakov.tm.api.service.model.IUserGraphGraphService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.system.AccessDeniedException;
import ru.inshakov.tm.model.SessionGraph;
import ru.inshakov.tm.model.UserGraph;
import ru.inshakov.tm.repository.model.SessionGraphGraphRepository;
import ru.inshakov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class SessionGraphService extends AbstractGraphService<SessionGraph> implements ISessionGraphGraphService {

    @NotNull
    private final IUserGraphGraphService userService;

    @NotNull
    private final IPropertyService propertyService;

    public SessionGraphService(
            @NotNull final IConnectionService connectionService,
            @NotNull IUserGraphGraphService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionGraph> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<SessionGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (SessionGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph add(@Nullable final SessionGraph entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            @NotNull final List<SessionGraph> sessionGraphs = repository.findAll();
            for (SessionGraph t :
                    sessionGraphs) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            @Nullable final SessionGraph sessionGraph = repository
                    .getReference(optionalId.orElseThrow(EmptyIdException::new));
            if (sessionGraph == null) return;
            repository.remove(sessionGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionGraph entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            @Nullable final SessionGraph sessionGraph = repository.getReference(entity.getId());
            if (sessionGraph == null) return;
            repository.remove(sessionGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    @NotNull
    public SessionGraph open(@Nullable final String login, @Nullable final String password) {
        @Nullable final UserGraph userGraph = checkDataAccess(login, password);
        if (userGraph == null) throw new AccessDeniedException();
        final SessionGraph sessionGraph = new SessionGraph();
        sessionGraph.setUserGraph(userGraph);
        sessionGraph.setTimestamp(System.currentTimeMillis());
        @Nullable final SessionGraph resultSessionGraph = sign(sessionGraph);
        add(resultSessionGraph);
        return resultSessionGraph;
    }

    @Override
    @SneakyThrows
    public UserGraph checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final UserGraph userGraph = userService.findByLogin(login);
        if (userGraph == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty() || userGraph.isLocked()) return null;
        if (hash.equals(userGraph.getPasswordHash())) {
            return userGraph;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final SessionGraph sessionGraph, final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(sessionGraph);
        @Nullable final String userId = sessionGraph.getUserGraph().getId();
        @Nullable final UserGraph userGraph = userService.findById(userId);
        if (userGraph == null) throw new AccessDeniedException();
        if (!role.equals(userGraph.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph sessionGraph) {
        if (sessionGraph == null) throw new AccessDeniedException();
        if (sessionGraph.getSignature() == null || sessionGraph.getSignature().isEmpty()) throw new AccessDeniedException();
        if (sessionGraph.getUserGraph().getId() == null || sessionGraph.getUserGraph().getId().isEmpty()) throw new AccessDeniedException();
        if (sessionGraph.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = sessionGraph.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = sessionGraph.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            if (repository.findById(sessionGraph.getId()) == null) throw new AccessDeniedException();
        } finally {
            entityManager.close();
        }

    }

    @Override
    @SneakyThrows
    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph sessionGraph) {
        if (sessionGraph == null) return null;
        sessionGraph.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, sessionGraph);
        sessionGraph.setSignature(signature);
        return sessionGraph;
    }

    @Override
    @SneakyThrows
    public void close(@Nullable final SessionGraph sessionGraph) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            @Nullable final SessionGraph sessionGraphReference = repository.getReference(sessionGraph.getId());
            if (sessionGraphReference == null) return;
            repository.remove(sessionGraphReference);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAllByUserId(@Nullable final String userId) {
        if (userId == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            @NotNull final List<SessionGraph> sessionGraphs = repository.findAllByUserId(userId);
            for (SessionGraph t :
                    sessionGraphs) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public List<SessionGraph> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionGraphGraphRepository repository = new SessionGraphGraphRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }
}
